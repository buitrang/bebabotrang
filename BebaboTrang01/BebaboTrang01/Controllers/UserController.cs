﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BebaboTrang01.Controllers
{
    public class UserController : Controller
    {
        private DAOUser daoUser = new DAOUser(); 
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Connection()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult Connection(string login, string password)
        {
            User unUser = daoUser.Connection(login, password);
            Session.Add("userConnected", unUser);
            return RedirectToAction("Index", "Profile");          
        }
        public ActionResult DeConnection()
        {
            Session["userConnected"] = null;           
            return RedirectToAction("Login", "Home");
        }
        [HttpGet]
        public ActionResult CreateUser(string message=null)
        {
            if (!string.IsNullOrEmpty(message) && message== "exitedUser")
            {
                ViewBag.UserExit = "***This email has been used!";
            }
            return View(); 
        }
        ///<summary>
        ///Check if the User-login is exited?
        ///then send JSON to view User/Create: 
        ///not yet exit=> connection then go to Profile/Index
        ///exited=> display a warning message
        ///</summary>
        public JsonResult UserCheck(string unLogin)
        {
            int check = 0; 
            List<User> listUser = daoUser.readAll();
            for (int i = 0; i < listUser.Count(); i++)
            {

                check += (listUser[i].Login == unLogin) ? 1:0 ; 
            }
            return Json(check, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Create(string login, string password)
        {
            User newUser = new User(login, password);          
      
            daoUser.create(newUser);
            Connection(login, password);
            return Redirect("~/Profile/Index?message=ok");            
            
        }
    
    }
}