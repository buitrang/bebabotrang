﻿using BebaboTrang01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using System.IO;

namespace BebaboTrang01.Controllers
{
    public class ProfileController : Controller
    {
        #region DAO

        private DAOUserRoleProfil daoUserRoleProfil = new DAOUserRoleProfil();
        private DAOCourbe daoCourbe = new DAOCourbe();
        private DAOProfile daoProfile = new DAOProfile();
        private DAOGenre daoGenre = new DAOGenre();
        private DAORole daoRole = new DAORole();
        private DAOProfileUserRelation daoProfileUserRelation = new DAOProfileUserRelation();
        private DAOUser daoUser = new DAOUser();

        #endregion
        // GET: Profile
        public ActionResult Index( string message=null)
        {
            ModelUserProfilRoleRelation userProfileRoleRelation = new ModelUserProfilRoleRelation();           

            userProfileRoleRelation.User = (User)Session["userConnected"];
            //Dictionary(Profile,List<role>)
            userProfileRoleRelation.RoleProfile = daoUserRoleProfil.readByUser(userProfileRoleRelation.User);
            foreach (KeyValuePair< ProfileFull, List<Role>> item in userProfileRoleRelation.RoleProfile)
            {
                foreach (var item2 in item.Value)
                {
                    if (item2.Description == "owner") Session.Add("person", item.Key.profile.Nom);
                    // used in View to clarify his/her own profile
                }

            }
            userProfileRoleRelation.RelationProfile = daoProfileUserRelation.readByUser(userProfileRoleRelation.User); 
            if (message != null)
            {
                ViewBag.Message = "CREATE Status:" +
                    "Welcome to YOUR BEBABO!!!Your user has been created!" +
                    "Now you can create your own profile or your family members' profiles!";
            }
            return View(userProfileRoleRelation);
        }
        [HttpGet]
        public ActionResult Create()
        {
            CreateProfile p = new CreateProfile();
            p.listGenre = daoGenre.readAll();
            return View(p);
        }
        public ActionResult CreateProfile()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(string nom, string prenom,  int fk_idGenre, HttpPostedFileBase nomFichier, string legende,
            int? poids, int? taille, int? perimetreCraien, string ddn=null,
            string dateMesure=null )
        {
            ProfileFull p = new ProfileFull();
            /// <summary>
            /// Create new profile into the DB
            /// </summary>  
            DateTime? birthday = (!string.IsNullOrEmpty(ddn))? Convert.ToDateTime(ddn): (DateTime?)null;
            p.profile = new Profile(nom, prenom, birthday, fk_idGenre);
           
            
            /// <summary>
            /// Create userRoleProfil
            /// Note phai read Profile to take idProfil
            /// </summary>  
            int idProfil= daoProfile.createReturnId(p.profile);
            int idRole = 1;
            User userConnected = (User)Session["userConnected"];
            UserRoleProfil dalURP = new UserRoleProfil(userConnected.IdUser, idRole, idProfil);             
            daoUserRoleProfil.create(dalURP); 
            
            /// <summary>
            /// Create new Courbe of this Profile into the DB
            /// </summary>  
            if (poids !=null || taille != null || perimetreCraien != null && dateMesure != null)
            {
                DateTime dMesure = Convert.ToDateTime(dateMesure);
                Courbe courbe = new Courbe(taille,poids,perimetreCraien, dMesure,idProfil);
                daoCourbe.create(courbe);
            }
            /// <summary>
            /// Add a photo's profile
            /// </summary> 
            string[] extentions = new[] { ".svg", ".png", ".jpg", ".jpeg", ".mp4" };
            string path = Server.MapPath("C:\xampp\htdocs\ProjetGestionDonnee\PROJETGESTION_SELF\bebabotrang\PhotoProfileUpload");
            string ext = Path.GetExtension(nomFichier.FileName); 
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path); 
            }
            if (nomFichier != null)
            {
                string fileName = idProfil.ToString()+ext;
                Path.Combine(path, fileName); 
                nomFichier.SaveAs(path+fileName); 
            }

           
            return RedirectToAction("Index", "Profile"); 
            //return RedirectToAction("Update","Profile", p); 
        }
        public ActionResult Update(Profile p)
        {

            return View(); 
        }
        [HttpGet]
        public ActionResult Partage(int? id)
        {
            FormulairePartage formulairePartage = new FormulairePartage();
            formulairePartage.listUser = daoUser.readAll();
            formulairePartage.listRole = daoRole.readAll();
            if (id !=null)
            {
                formulairePartage.idProfile = (int)id;
                formulairePartage.userListRole = daoUserRoleProfil.readByIdProfile((int)id);
            }            
            
            return View(formulairePartage); 
        }
        [HttpPost]
        public ActionResult Partage(string login, int idRole, int idProfile)
        {
            User unUser = daoUser.readByLogin(login);
            UserRoleProfil userRoleProfil = new UserRoleProfil(unUser.IdUser, idRole, idProfile); 
            daoUserRoleProfil.create(userRoleProfil);
            return Redirect("/Profile/Partage?id="+idProfile);  
        }

    }
}