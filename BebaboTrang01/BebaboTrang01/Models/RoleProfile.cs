﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BebaboTrang01.Models
{
    public class RoleProfile
    {
        public ProfileFull profileFull;
        public List<Role> listRole;

        public RoleProfile()
        {

        }
        public RoleProfile( ProfileFull unProfileFull, List<Role> unListRole)
        {
            profileFull = unProfileFull;
            listRole = unListRole; 
        }
       
    }
}