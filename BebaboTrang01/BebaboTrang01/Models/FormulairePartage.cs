﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations; 

namespace BebaboTrang01.Models
{
    public class FormulairePartage
    {
        [Required(ErrorMessage = "this input is required")]
        [EmailAddress]
        public string login { get; set; }

        public int idProfile { get; set;  }
        public  List<User> listUser { get; set; }
        public  List<Role> listRole { get; set; } 
        public  Dictionary<User, List<Role>> userListRole { get; set; }
    }
}