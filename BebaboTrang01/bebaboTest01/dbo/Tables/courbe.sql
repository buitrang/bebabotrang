﻿CREATE TABLE [dbo].[courbe] (
    [poids]            INT      NOT NULL,
    [idCourbe]         INT      IDENTITY (1, 1) NOT NULL,
    [taille]           INT      NULL,
    [dateMesure]       DATETIME NOT NULL,
    [perimetreCranien] INT      NULL,
    [fk_idProfil]      INT      NULL,
    CONSTRAINT [PK_courbe] PRIMARY KEY CLUSTERED ([idCourbe] ASC),
    CONSTRAINT [FK_courbe_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil])
);

