﻿CREATE TABLE [dbo].[genre] (
    [idGenre] INT          NOT NULL,
    [genre]   VARCHAR (50) NULL,
    CONSTRAINT [PK_genre] PRIMARY KEY CLUSTERED ([idGenre] ASC)
);

