﻿CREATE TABLE [dbo].[profil] (
    [idProfil]   INT          IDENTITY (1, 1) NOT NULL,
    [nom]        VARCHAR (50) NULL,
    [prenom]     VARCHAR (50) NULL,
    [ddn]        DATE         NULL,
    [fk_idGenre] INT          NULL,
    CONSTRAINT [PK_timeline] PRIMARY KEY CLUSTERED ([idProfil] ASC),
    CONSTRAINT [FK_profil_genre] FOREIGN KEY ([fk_idGenre]) REFERENCES [dbo].[genre] ([idGenre])
);

