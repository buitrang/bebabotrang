﻿CREATE TABLE [dbo].[relation] (
    [idRelation] INT          IDENTITY (1, 1) NOT NULL,
    [libelle]    VARCHAR (50) NULL,
    CONSTRAINT [PK_relation] PRIMARY KEY CLUSTERED ([idRelation] ASC)
);

