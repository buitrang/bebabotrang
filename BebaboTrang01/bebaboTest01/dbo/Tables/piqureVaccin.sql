﻿CREATE TABLE [dbo].[piqureVaccin] (
    [idPiqureVaccin]          INT NOT NULL,
    [fk_idVaccinLotFoisDuree] INT NULL,
    [fk_idProfil]             INT NULL,
    CONSTRAINT [PK_piqureVaccin] PRIMARY KEY CLUSTERED ([idPiqureVaccin] ASC),
    CONSTRAINT [FK_piqureVaccin_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil]),
    CONSTRAINT [FK_piqureVaccin_vaccinLotFoisDuree] FOREIGN KEY ([fk_idVaccinLotFoisDuree]) REFERENCES [dbo].[vaccinLotFoisDuree] ([idVaccinLotFoisDuree])
);

