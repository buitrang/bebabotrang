﻿CREATE TABLE [dbo].[traitementMaladie] (
    [idTraitementMaladie]       INT          NOT NULL,
    [fk_idMaladie]              INT          NULL,
    [dateMaladie]               DATE         NULL,
    [fk_idTraitementMedicament] INT          NULL,
    [type]                      VARCHAR (50) NULL,
    [fk_idProfil]               INT          NULL,
    CONSTRAINT [PK_traitementMaladie] PRIMARY KEY CLUSTERED ([idTraitementMaladie] ASC),
    CONSTRAINT [FK_traitementMaladie_maladie] FOREIGN KEY ([fk_idMaladie]) REFERENCES [dbo].[maladie] ([idMaladie]),
    CONSTRAINT [FK_traitementMaladie_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil]),
    CONSTRAINT [FK_traitementMaladie_traitementMedicament] FOREIGN KEY ([fk_idTraitementMedicament]) REFERENCES [dbo].[traitementMedicament] ([idTraitementMedicament])
);

