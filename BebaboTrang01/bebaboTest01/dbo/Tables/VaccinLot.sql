﻿CREATE TABLE [dbo].[VaccinLot] (
    [idVaccinLot] INT          NOT NULL,
    [nomVaccin]   VARCHAR (50) NULL,
    [refVaccin]   VARCHAR (50) NULL,
    CONSTRAINT [PK_VaccinLot] PRIMARY KEY CLUSTERED ([idVaccinLot] ASC)
);

