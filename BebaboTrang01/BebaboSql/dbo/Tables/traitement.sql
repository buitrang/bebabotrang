﻿CREATE TABLE [dbo].[traitement] (
    [idTraitement]    INT           NOT NULL,
    [noteTraitement]  VARCHAR (100) NULL,
    [dateDebut]       DATE          NULL,
    [dateFin]         DATE          NULL,
    [fk_idMedicament] INT           NULL,
    CONSTRAINT [PK_traitement] PRIMARY KEY CLUSTERED ([idTraitement] ASC)
);

