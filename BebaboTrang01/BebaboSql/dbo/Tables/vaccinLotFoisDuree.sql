﻿CREATE TABLE [dbo].[vaccinLotFoisDuree] (
    [idVaccinLotFoisDuree] INT NOT NULL,
    [fk_idVaccinLot]       INT NULL,
    [fk_idFois]            INT NULL,
    [fk_idDuree]           INT NULL,
    CONSTRAINT [PK_vaccinLotFoisDuree] PRIMARY KEY CLUSTERED ([idVaccinLotFoisDuree] ASC),
    CONSTRAINT [FK_vaccinLotFoisDuree_duree] FOREIGN KEY ([fk_idDuree]) REFERENCES [dbo].[duree] ([idDuree]),
    CONSTRAINT [FK_vaccinLotFoisDuree_fois] FOREIGN KEY ([fk_idFois]) REFERENCES [dbo].[fois] ([idFois]),
    CONSTRAINT [FK_vaccinLotFoisDuree_VaccinLot] FOREIGN KEY ([fk_idVaccinLot]) REFERENCES [dbo].[VaccinLot] ([idVaccinLot])
);

