﻿CREATE TABLE [dbo].[profile] (
    [idProfile]     INT          NOT NULL,
    [nom]           VARCHAR (50) NULL,
    [prenom]        VARCHAR (50) NULL,
    [ddn]           DATE         NULL,
    [fk_idGenre]    INT          NOT NULL,
    [fk_idTimeline] INT          NULL,
    [fk_idUser]     INT          NULL,
    CONSTRAINT [PK_profile_1] PRIMARY KEY CLUSTERED ([idProfile] ASC),
    CONSTRAINT [FK_profile_genre1] FOREIGN KEY ([fk_idGenre]) REFERENCES [dbo].[genre] ([idGenre]),
    CONSTRAINT [FK_profile_timeline] FOREIGN KEY ([fk_idTimeline]) REFERENCES [dbo].[profil] ([idProfil]),
    CONSTRAINT [FK_profile_user] FOREIGN KEY ([fk_idUser]) REFERENCES [dbo].[user] ([idUser])
);

