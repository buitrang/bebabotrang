﻿CREATE TABLE [dbo].[maladie] (
    [idMaladie]  INT           NOT NULL,
    [nomMaladie] VARCHAR (100) NULL,
    CONSTRAINT [PK_maladie] PRIMARY KEY CLUSTERED ([idMaladie] ASC)
);

