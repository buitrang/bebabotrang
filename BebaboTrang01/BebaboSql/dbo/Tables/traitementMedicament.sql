﻿CREATE TABLE [dbo].[traitementMedicament] (
    [idTraitementMedicament] INT NOT NULL,
    [fk_idTraitement]        INT NULL,
    [fk_idMedicament]        INT NULL,
    CONSTRAINT [PK_traitementMedicament] PRIMARY KEY CLUSTERED ([idTraitementMedicament] ASC),
    CONSTRAINT [FK_traitementMedicament_medicament] FOREIGN KEY ([fk_idMedicament]) REFERENCES [dbo].[medicament] ([idMedicament]),
    CONSTRAINT [FK_traitementMedicament_traitement] FOREIGN KEY ([fk_idTraitement]) REFERENCES [dbo].[traitement] ([idTraitement])
);

