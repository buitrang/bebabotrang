﻿CREATE TABLE [dbo].[MaladieVaccinLot] (
    [idMaladieVaccinLot] INT NOT NULL,
    [fk_idMaladie]       INT NULL,
    [fk_idVaccinLot]     INT NULL,
    CONSTRAINT [PK_MaladieVaccinLot] PRIMARY KEY CLUSTERED ([idMaladieVaccinLot] ASC),
    CONSTRAINT [FK_MaladieVaccinLot_maladie] FOREIGN KEY ([fk_idMaladie]) REFERENCES [dbo].[maladie] ([idMaladie]),
    CONSTRAINT [FK_MaladieVaccinLot_VaccinLot] FOREIGN KEY ([fk_idVaccinLot]) REFERENCES [dbo].[VaccinLot] ([idVaccinLot])
);

