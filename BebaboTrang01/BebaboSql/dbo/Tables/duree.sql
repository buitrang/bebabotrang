﻿CREATE TABLE [dbo].[duree] (
    [idDuree] INT NOT NULL,
    [duree]   INT NULL,
    CONSTRAINT [PK_duree] PRIMARY KEY CLUSTERED ([idDuree] ASC)
);

