﻿CREATE TABLE [dbo].[profilUserRelation] (
    [idProfilUserRelation] INT NOT NULL,
    [fk_idProfil]          INT NULL,
    [fk_idUser]            INT NULL,
    [fk_idRelation]        INT NULL,
    CONSTRAINT [PK_profilUserRelation] PRIMARY KEY CLUSTERED ([idProfilUserRelation] ASC),
    CONSTRAINT [FK_profilUserRelation_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil]),
    CONSTRAINT [FK_profilUserRelation_relation] FOREIGN KEY ([fk_idRelation]) REFERENCES [dbo].[relation] ([idRelation]),
    CONSTRAINT [FK_profilUserRelation_user] FOREIGN KEY ([fk_idUser]) REFERENCES [dbo].[user] ([idUser])
);

