﻿CREATE TABLE [dbo].[relation] (
    [idRelation] INT          NOT NULL,
    [libelle]    VARCHAR (50) NULL,
    CONSTRAINT [PK_relation] PRIMARY KEY CLUSTERED ([idRelation] ASC)
);

