﻿CREATE TABLE [dbo].[userRoleProfil] (
    [idUserRoleProfil] INT NOT NULL,
    [fk_idUser]        INT NULL,
    [fk_idRole]        INT NULL,
    [fk_idProfil]      INT NULL,
    CONSTRAINT [PK_userRoleProfil] PRIMARY KEY CLUSTERED ([idUserRoleProfil] ASC),
    CONSTRAINT [FK_userRoleProfil_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil]),
    CONSTRAINT [FK_userRoleProfil_roles] FOREIGN KEY ([fk_idRole]) REFERENCES [dbo].[roles] ([idRole]),
    CONSTRAINT [FK_userRoleProfil_user] FOREIGN KEY ([fk_idUser]) REFERENCES [dbo].[user] ([idUser])
);

