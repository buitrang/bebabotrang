﻿CREATE TABLE [dbo].[terme] (
    [idTerme]       INT          NOT NULL,
    [nombreSemaine] INT          NULL,
    [noteTerme]     VARCHAR (50) NULL,
    CONSTRAINT [PK_terme] PRIMARY KEY CLUSTERED ([idTerme] ASC)
);

