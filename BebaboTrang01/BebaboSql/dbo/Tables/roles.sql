﻿CREATE TABLE [dbo].[roles] (
    [idRole]      INT          NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED ([idRole] ASC)
);

