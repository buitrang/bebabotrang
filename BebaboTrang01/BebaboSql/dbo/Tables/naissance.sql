﻿CREATE TABLE [dbo].[naissance] (
    [idNaissance]           INT          NOT NULL,
    [lieuNaissance]         VARCHAR (50) NULL,
    [noteNaissance]         VARCHAR (50) NULL,
    [fk_idTerme]            INT          NULL,
    [fk_idTypeAccouchement] INT          NULL,
    [fk_idTest]             INT          NULL,
    [fk_idProfil]           INT          NULL,
    CONSTRAINT [PK_naissance] PRIMARY KEY CLUSTERED ([idNaissance] ASC),
    CONSTRAINT [FK_naissance_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil]),
    CONSTRAINT [FK_naissance_terme] FOREIGN KEY ([fk_idTerme]) REFERENCES [dbo].[terme] ([idTerme]),
    CONSTRAINT [FK_naissance_test] FOREIGN KEY ([fk_idTest]) REFERENCES [dbo].[test] ([idTest]),
    CONSTRAINT [FK_naissance_typeAccouchement] FOREIGN KEY ([fk_idTypeAccouchement]) REFERENCES [dbo].[typeAccouchement] ([idTypeAccouchement])
);

