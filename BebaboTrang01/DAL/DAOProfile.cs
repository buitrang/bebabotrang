﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOProfile : DAO, IDAO<Profile>
    {
        DAOCourbe daoCourbe = new DAOCourbe();
        public Profile create(Profile obj) {
            throw new NotImplementedException();
        }
        public int createReturnId(Profile obj)
        {
            SqlCommand command = connection.CreateCommand();            
            string query = "INSERT INTO " +
                "profil (nom, prenom, ddn, fk_idGenre) " +
                "OUTPUT INSERTED.idProfil " +
                "VALUES (@nom, @prenom, @ddn, @fk_idGenre)";
            command.CommandText = query;
            command.Parameters.AddWithValue("@nom", obj.Nom);
            command.Parameters.AddWithValue("@prenom", obj.Prenom);
            command.Parameters.AddWithValue("@ddn", obj.Ddn);
            command.Parameters.AddWithValue("@fk_idGenre", obj.Fk_idGenre);

            connection.Open();
            int idUser = (int)command.ExecuteScalar();
            connection.Close();

            return idUser; 
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Profile read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM profil where idProfil = "+id;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Profile profile = null;
            foreach (DataRow row in dt.Rows)
            {
                profile = new Profile(
                    (int)row["idProfil"],
                    row["nom"].ToString(),
                    row["prenom"].ToString(), 
                    (DateTime)row["ddn"], 
                    (int)row["fk_idGenre"]
                    );               
            }
            return profile;             
        }
        public Profile readByUser(User user)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM profil where idProfil = " + user.IdUser;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Profile profile = null;
            foreach (DataRow row in dt.Rows)
            {
                profile = new Profile(
                    (int)row["idProfil"],
                    row["nom"].ToString(),
                    row["prenom"].ToString(),
                    (DateTime)row["ddn"],
                    (int)row["fk_idGenre"]
                    );
                //profile.listCourbe = daoCourbe.readByIdProfile((int)row["idProfil"]);
            }
            return profile;

        }
       
        
        public List<Profile> readAll()
        {
            throw new NotImplementedException();
        }

        public Profile update(Profile obj)
        {
            throw new NotImplementedException();
        }
    }
}
