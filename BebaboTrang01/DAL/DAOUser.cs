﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOUser : DAO
    {
        public User create(User obj)
        {
            SqlCommand command = connection.CreateCommand() ;
            //string query = "INSERT INTO " +
            //    "[user] (login, password) " +
            //    //"OUTPUT INSERTED.idUser " +
            //    "VALUES (@login, @password); SELECT SCOPE_IDENTITY()"; 
            string query = "INSERT INTO " +
                "[user] (login, password) " +
                "OUTPUT INSERTED.idUser " +
                "VALUES (@login, @password)";
            command.CommandText = query;
            command.Parameters.AddWithValue("@login", obj.Login);
            command.Parameters.AddWithValue("@password", obj.Password);

            connection.Open();
            int idUser = (int)command.ExecuteScalar();           
            connection.Close();

            return obj; 
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        //public UserRoleProfil read(int id)
        //{
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// this function to find User by id. 
        /// User in DAOUserRoleProfil.cs to create dictionary(user, list<role>)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User read(int id)
        {
            SqlCommand command = connection.CreateCommand() ;
            string query = "SELECT * FROM [user] WHERE idUser= "+ id;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            da.SelectCommand = command;
            da.Fill(dt);
            User unUser = null; 
            foreach (DataRow row in dt.Rows)
            {
                unUser = new User(
                    (int)row["idUser"],
                    row["login"].ToString(),
                    row["password"].ToString()
                    );
            }
            //connection.Open(); 
            //SqlDataReader reader = command.ExecuteReader();
            //User unUser = null; 
            //while (reader.Read())
            //{
            //    unUser = new User(
            //        (int)reader["idUser"],
            //        reader["login"].ToString(), 
            //        reader["password"].ToString()
            //        ); 
            //}
            //connection.Close(); 
            return unUser; 
        }
        public User readByLogin(string login)
        {
           
            User unUser = null;
            SqlCommand command = connection.CreateCommand();             
            string query = "SELECT * FROM [user] WHERE login= @login";
            command.CommandText = query;
            command.Parameters.AddWithValue("@login", login); 
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            da.SelectCommand = command;
            da.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                unUser = new User(
                     (int)row["idUser"],
                    row["login"].ToString(),
                    row["password"].ToString()
                    );
            }
         
            return unUser; 
        }



        public List<User> readAll()
        {
            List<User> listUser = new List<User>();
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM [user]";
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();            
            da.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                User unUser = new User(
                    (int)row["idUser"],
                    row["login"].ToString(),
                    row["password"].ToString());
                listUser.Add(unUser); 
            }
            return listUser; 
        }

        public UserRoleProfil update(UserRoleProfil obj)
        {
            throw new NotImplementedException();
        }
        public User Connection(string login, string password)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM [user] WHERE login = @login AND password = @password ";
            command.CommandText = query;
            command.Parameters.AddWithValue("@login", login);
            command.Parameters.AddWithValue("@password", password);
            
            int id;
            try
            {
                connection.Open();
                id = (int)command.ExecuteScalar();                
                return new User(id, login, password); 
            }
            catch (Exception ex)
            {
                connection.Close();
                return null;
            }
            finally
            {
                connection.Close();
            }
            if (id == null) return null;

        }
    }
}
