﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOProfileFull : DAO, IDAO<ProfileFull>
    {
        private DAOProfile daoProfile = new DAOProfile();
        private DAOCourbe daoCourbe = new DAOCourbe(); 

        public ProfileFull create(ProfileFull obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public ProfileFull read(int id)
        {
            ProfileFull profileFull = new ProfileFull() ;
            profileFull.profile= daoProfile.read(id);
            profileFull.listCourbe = daoCourbe.readByIdProfile(id); 
            return profileFull; 
        }

        public List<ProfileFull> readAll()
        {
            throw new NotImplementedException();
        }

        public ProfileFull update(ProfileFull obj)
        {
            throw new NotImplementedException();
        }
    }
}
