﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class User
    {
        public int IdUser { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public User()
        {

        }
        public User(string unLogin, string unPassword)
        {
            this.Login = unLogin;
            this.Password = unPassword;
        }
        public User(int idUser, string unLogin, string unPassword): this(unLogin, unPassword)
        {
            this.IdUser = idUser; 
        }
    }
}
