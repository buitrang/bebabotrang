﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient; 

namespace DAL
{
    public class DAORelation : DAO, IDAO<Relation>
    {
        public Relation create(Relation obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Relation read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM relation where idRelation = " + id;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Relation relation = null;
            foreach (DataRow row in dt.Rows)
            {
                relation = new Relation(
                    (int)row["idRelation"],
                    row["libelle"].ToString()
                    );
            }
            return relation;
        }

        public List<Relation> readAll()
        {
            throw new NotImplementedException();
        }

        public Relation update(Relation obj)
        {
            throw new NotImplementedException();
        }
    }
}
