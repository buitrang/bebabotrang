﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Genre
    {
        private int _idGenre;

        public int IdGenre
        {
            get { return _idGenre; }
            set { _idGenre = value; }
        }
        private string _genre;

        public string GenreType
        {
            get { return _genre; }
            set { _genre = value; }
        }
        public Genre()
        {

        }
        public Genre(int idGenre, string genre)
        {
            IdGenre = idGenre;
            GenreType = genre; 
        }

    }
}
