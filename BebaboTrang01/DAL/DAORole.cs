﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAORole : DAO, IDAO<Role>
    {
        public Role create(Role obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Role read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM roles where idRole = " + id;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            Role role = null;
            foreach (DataRow row in dt.Rows)
            {
                role = new Role(
                    (int)row["idRole"],
                    row["Description"].ToString()
                    );
            }
            return role;
        }

        public List<Role> readAll()
        {
            List<Role> listRole = new List<Role>();
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM roles";
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                Role unRole = new Role(
                    (int)row["idRole"],
                    row["Description"].ToString());
                listRole.Add(unRole);
            }
            return listRole;
        }

        public Role update(Role obj)
        {
            throw new NotImplementedException();
        }
    }
}
