﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserRoleProfil
    {
        private int _idUserRoleProfil;
        public int IdUserRoleProfil
        {
            get { return _idUserRoleProfil; }
            set { _idUserRoleProfil = value; }
        }

        private int _fk_idUser;
        public int Fk_idUser
        {
            get { return _fk_idUser; }
            set { _fk_idUser = value; }
        }

        private int _fk_idRole;
        public int Fk_idRole
        {
            get { return _fk_idRole; }
            set { _fk_idRole = value; }
        }

        private int _fk_idProfil;

        public int Fk_idProfil
        {
            get { return _fk_idProfil; }
            set { _fk_idProfil = value; }
        }

        public UserRoleProfil()
        {

        }
        public UserRoleProfil(int idUser, int idRole, int idProfil)
        {
            Fk_idProfil = idProfil;
            Fk_idRole = idRole;
            Fk_idUser = idUser; 
        }




    }
}
