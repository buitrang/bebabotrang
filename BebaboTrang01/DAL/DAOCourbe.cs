﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOCourbe : DAO, IDAO<Courbe>
    {
        public Courbe create(Courbe obj)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "INSERT INTO " +
                "courbe (poids, taille, dateMesure, perimetreCranien, fk_idProfil) " +
                "OUTPUT INSERTED.idCourbe " +
                "VALUES (@poids, @taille, @dateMesure, @perimetreCranien, @fk_idProfil)";
            command.CommandText = query;
            command.Parameters.AddWithValue("@poids", obj.Poids);
            command.Parameters.AddWithValue("@taille", obj.Taille);
            command.Parameters.AddWithValue("@dateMesure", obj.DateMesure);
            command.Parameters.AddWithValue("@perimetreCranien", obj.PerimetreCraien);
            command.Parameters.AddWithValue("@fk_idProfil", obj.Fk_idProfil); 

            connection.Open();
            int idCourbe = (int)command.ExecuteScalar();
            connection.Close();

            return obj;
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }
        public Courbe read(int id)
        {
            throw new NotImplementedException();
        }

        public List<Courbe> readByIdProfile(int idProfile)
        {
            List<Courbe> listCourbe = new List<Courbe>(); 
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM courbe WHERE fk_idProfil = "+ idProfile;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                Courbe unCourbe = new Courbe(
                    (int)row["idCourbe"],
                    (int)row["taille"],
                    (int)row["poids"],
                    (int)row["perimetreCranien"],
                    (DateTime)row["dateMesure"], 
                    (int)row["fk_idProfil"]
                    );
                listCourbe.Add(unCourbe); 
            }            

            return listCourbe; 
        }
        

        public List<Courbe> readAll()
        {
            throw new NotImplementedException();
        }

        public Courbe update(Courbe obj)
        {
            throw new NotImplementedException();
        }
    }
}
