﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOGenre : DAO, IDAO<Genre>
    {
        public Genre create(Genre obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Genre read(int id)
        {
            throw new NotImplementedException();
        }

        public List<Genre> readAll()
        {
            List<Genre> listGenre = new List<Genre>() ;
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM genre";
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            da.SelectCommand = command;
            da.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                Genre genre = new Genre(
                    (int)row["idGenre"], 
                    row["genre"].ToString()
                    );
                listGenre.Add(genre); 
            }
            return listGenre; 
        }

        public Genre update(Genre obj)
        {
            throw new NotImplementedException();
        }
    }
}
