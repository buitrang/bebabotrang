﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
     public class Role
    {
        public int IdRole;
        public string Description;
        public Role()
        {

        }
        public Role(string description)
        {            
            Description = description;
        }
        public Role(int idRole, string description): this(description)
        {
            IdRole = idRole;           
        }
    }
}
