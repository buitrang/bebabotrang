﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOUserRoleProfil : DAO, IDAO<UserRoleProfil>
    {
        private DAOProfile daoProfile = new DAOProfile(); 
        private DAORole daoRole = new DAORole();
        private DAOUser daoUser = new DAOUser();
        private DAOProfileFull daoProfileFull = new DAOProfileFull(); 

        public UserRoleProfil create(UserRoleProfil obj)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "INSERT INTO " +
                "userRoleProfil (fk_idUser, fk_idRole, fk_idProfil) " +
                "OUTPUT INSERTED.idUserRoleProfil " +
                "VALUES (@fk_idUser, @fk_idRole, @fk_idProfil)";
            command.CommandText = query;
            command.Parameters.AddWithValue("@fk_idUser", obj.Fk_idUser);
            command.Parameters.AddWithValue("@fk_idRole", obj.Fk_idRole);
            command.Parameters.AddWithValue("@fk_idProfil", obj.Fk_idProfil);
           

            connection.Open();
            int userRoleProfil = (int)command.ExecuteScalar();
            connection.Close();
            return obj; 
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public UserRoleProfil read(int id)
        {
            throw new NotImplementedException();
        }
       /// <summary>
       /// A user has many roles for each profile. Thus we have dictionary is a list of role for each profile
       /// </summary>
       /// <param name="user"></param>
       /// <returns></returns>
        public Dictionary<ProfileFull, List<Role>> readByUser(User user)
        {
            
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM userRoleProfil WHERE fk_idUser = @fk_idUser";
            command.CommandText = query;
            command.Parameters.AddWithValue("@fk_idUser", user.IdUser);            
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();            
            da.Fill(dt);

            Dictionary<ProfileFull, List < Role >> listRoleProfile = new Dictionary<ProfileFull, List<Role>>();
            Dictionary<Role, ProfileFull> roleProfile = new Dictionary<Role, ProfileFull>(); 

            //Add each row to an dictionary(role, profile): 
            foreach (DataRow row in dt.Rows)
            {          
                roleProfile.Add(daoRole.read((int)row["fk_idRole"]), daoProfileFull.read((int)row["fk_idProfil"])); 
            }

            //group dictionary(role, profile) all roles ToList in a dictionary(profile, list<role>): 
            listRoleProfile = roleProfile.GroupBy(r => r.Value).ToDictionary(t=>t.Key, t=>t.Select(r=>r.Key).ToList()); 
            return listRoleProfile; 
        }

        /// <summary>
        /// Function to read by IdProfile in table userRoleProfile        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Dictionary<User, List<Role>></returns>
        public Dictionary<User, List<Role>> readByIdProfile(int id)
        {

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM userRoleProfil WHERE fk_idProfil = @fk_idProfil";
            command.CommandText = query;
            command.Parameters.AddWithValue("@fk_idProfil", id);
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Dictionary<User, List<Role>> userListRole = new Dictionary<User, List<Role>>();
            Dictionary<Role, User> roleUser = new Dictionary<Role, User>();

            //Add each row to an dictionary(role, profile): 
            foreach (DataRow row in dt.Rows)
            {
                roleUser.Add(daoRole.read((int)row["fk_idRole"]), daoUser.read((int)row["fk_idUser"]));
            }

            //group dictionary(role, profile) all roles ToList in a dictionary(profile, list<role>): 
            userListRole = roleUser.GroupBy(r => r.Value).ToDictionary(t => t.Key, t => t.Select(r => r.Key).ToList());
            return userListRole;
        }

        public List<UserRoleProfil> readAll()
        {
            throw new NotImplementedException();
        }

        public UserRoleProfil update(UserRoleProfil obj)
        {
            throw new NotImplementedException();
        }
    }
}
