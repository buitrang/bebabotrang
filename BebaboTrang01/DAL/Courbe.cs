﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Courbe
    {
        #region propfull

        private int _idCourbe;
        public int IdCourbe
        {
            get { return _idCourbe; }
            set { _idCourbe = value; }
        }

        private int? _taille;
        public int? Taille
        {
            get { return _taille; }
            set { _taille = value; }
        }

        private int? _poids;
        public int? Poids
        {
            get { return _poids; }
            set { _poids = value; }
        }

        private int? _perimetreCranien;
        public int? PerimetreCraien
        {
            get { return _perimetreCranien; }
            set { _perimetreCranien = value; }
        }

        private DateTime _dateMesure;
        public DateTime DateMesure
        {
            get { return _dateMesure; }
            set { _dateMesure = value; }
        }

        public int Fk_idProfil; 

        #endregion

        #region Ctor
        public Courbe()
        {

        }
        public Courbe(int? taille, int? poids, int? perimetreCranien, DateTime dateMesure, int fk_idProfil)
        {          
            Taille = taille;
            Poids = poids;
            PerimetreCraien = perimetreCranien;
            DateMesure = dateMesure;
            Fk_idProfil = fk_idProfil; 
        }
        public Courbe(int id, int? taille, int? poids, int? perimetreCranien, DateTime dateMesure, int fk_idProfil)
            :this(taille, poids, perimetreCranien, dateMesure, fk_idProfil)
        {
            IdCourbe = id;           
        }
        #endregion




    }
}
