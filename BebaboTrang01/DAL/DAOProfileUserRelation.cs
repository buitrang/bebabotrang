﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAOProfileUserRelation : DAO, IDAO<User>
    {
        private DAORelation daoRelation = new DAORelation();
        private DAOProfile daoProfile = new DAOProfile();
        private DAOProfileFull daoProfileFull = new DAOProfileFull(); 

        public User create(User obj)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public User read(int id)
        {
            throw new NotImplementedException();
        }
        public Dictionary<Relation, ProfileFull> readByUser(User user)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM profilUserRelation WHERE fk_idUser = @fk_idUser";
            command.CommandText = query;
            command.Parameters.AddWithValue("@fk_idUser", user.IdUser);
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            Dictionary<Relation, ProfileFull> relationProfile = new Dictionary<Relation, ProfileFull>();
            foreach (DataRow row in dt.Rows)
            {

                relationProfile.Add(daoRelation.read((int)row["fk_idRelation"]), daoProfileFull.read((int)row["fk_idProfil"]));

            }
            return relationProfile;
        }
        
        public List<User> readAll()
        {
            throw new NotImplementedException();
        }

        public User update(User obj)
        {
            throw new NotImplementedException();
        }
    }
}
