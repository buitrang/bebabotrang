﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Profile
    {
        public int IdProfil;
        public string Nom;
        public string Prenom;     
        public DateTime? Ddn;
        public int Fk_idGenre;
        //public List<Courbe> listCourbe;
      



        public Profile()
        {

        }
        public Profile( string nom, string prenom, DateTime? ddn, int fk_idGenre)
        {            
            Nom = nom;
            Prenom = prenom;
            Ddn = ddn;
            Fk_idGenre = fk_idGenre;
        }
        public Profile(int id, string nom, string prenom, DateTime? ddn,int fk_idGenre )
            :this(nom, prenom, ddn, fk_idGenre)
        {
            IdProfil = id;           
        }

    }
}
