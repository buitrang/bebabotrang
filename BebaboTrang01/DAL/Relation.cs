﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Relation
    {
        private int idRelation;
        public int IdRelation
        {
            get { return idRelation; }
            set { idRelation = value; }
        }

        private string libelle;
        public string Libelle
        {
            get { return libelle; }
            set { libelle = value; }
        }

        public Relation()
        {

        }
        public Relation(string libelle)
        {
            Libelle = libelle; 
        }
        public Relation(int id, string libelle): this(libelle)
        {
            IdRelation = id; 
        }



    }
}
