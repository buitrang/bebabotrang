﻿CREATE TABLE [dbo].[test] (
    [idTest]   INT           NOT NULL,
    [nomTest]  VARCHAR (50)  NULL,
    [resultat] VARCHAR (100) NULL,
    CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED ([idTest] ASC)
);

