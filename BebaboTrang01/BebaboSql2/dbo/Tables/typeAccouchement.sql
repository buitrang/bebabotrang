﻿CREATE TABLE [dbo].[typeAccouchement] (
    [idTypeAccouchement]  INT          NOT NULL,
    [nomTypeAccouchement] VARCHAR (50) NULL,
    CONSTRAINT [PK_typeAccouchement] PRIMARY KEY CLUSTERED ([idTypeAccouchement] ASC)
);

