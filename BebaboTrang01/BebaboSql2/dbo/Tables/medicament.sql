﻿CREATE TABLE [dbo].[medicament] (
    [idMedicament]  INT          NOT NULL,
    [nomMedicament] VARCHAR (50) NULL,
    CONSTRAINT [PK_medicament] PRIMARY KEY CLUSTERED ([idMedicament] ASC)
);

