﻿CREATE TABLE [dbo].[user] (
    [idUser]   INT          IDENTITY (1, 1) NOT NULL,
    [login]    VARCHAR (50) NULL,
    [password] VARCHAR (50) NULL,
    CONSTRAINT [PK_user_1] PRIMARY KEY CLUSTERED ([idUser] ASC)
);

