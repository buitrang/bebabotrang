﻿CREATE TABLE [dbo].[memoire] (
    [idMemoire]       INT          NOT NULL,
    [nomEvenement]    VARCHAR (50) NULL,
    [noteEvenement]   VARCHAR (50) NULL,
    [fk_idPhotoVideo] INT          NULL,
    [fk_idProfil]     INT          NULL,
    CONSTRAINT [PK_memoire] PRIMARY KEY CLUSTERED ([idMemoire] ASC),
    CONSTRAINT [FK_memoire_photoVideo] FOREIGN KEY ([fk_idPhotoVideo]) REFERENCES [dbo].[photoVideo] ([idPhotoVideo]),
    CONSTRAINT [FK_memoire_profil] FOREIGN KEY ([fk_idProfil]) REFERENCES [dbo].[profil] ([idProfil])
);

