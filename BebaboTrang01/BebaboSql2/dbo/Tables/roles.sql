﻿CREATE TABLE [dbo].[roles] (
    [idRole]      INT          IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED ([idRole] ASC)
);

