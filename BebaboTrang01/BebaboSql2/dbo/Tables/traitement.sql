﻿CREATE TABLE [dbo].[traitement] (
    [idTraitement]    INT           NOT NULL,
    [noteTraitement]  VARCHAR (100) NULL,
    [dateDebut]       DATETIME      NULL,
    [dateFin]         DATETIME      NULL,
    [fk_idMedicament] INT           NULL,
    CONSTRAINT [PK_traitement] PRIMARY KEY CLUSTERED ([idTraitement] ASC)
);

