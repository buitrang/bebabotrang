﻿CREATE TABLE [dbo].[allergieMaladie] (
    [idAllergieMaladie]                INT          NOT NULL,
    [nomMaladie]                       VARCHAR (50) NULL,
    [dateMaladie]                      DATE         NULL,
    [fk_idTraitement]                  INT          NULL,
    [fk_idChroniquePonctuelleAllergie] INT          NULL,
    CONSTRAINT [PK_allergieMaladie] PRIMARY KEY CLUSTERED ([idAllergieMaladie] ASC),
    CONSTRAINT [FK_allergieMaladie_chroniquePonctuelleAllergie] FOREIGN KEY ([fk_idChroniquePonctuelleAllergie]) REFERENCES [dbo].[chroniquePonctuelleAllergie] ([idChroniquePonctuelleAllergie]),
    CONSTRAINT [FK_allergieMaladie_traitement] FOREIGN KEY ([fk_idTraitement]) REFERENCES [dbo].[traitement] ([idTraitement])
);

