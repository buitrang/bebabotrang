﻿CREATE TABLE [dbo].[heritage] (
    [idHeritage] INT NOT NULL,
    [enfant]     INT NOT NULL,
    [parent1]    INT NULL,
    [parent2]    INT NULL,
    CONSTRAINT [PK_heritage] PRIMARY KEY CLUSTERED ([idHeritage] ASC),
    CONSTRAINT [FK_heritage_user] FOREIGN KEY ([enfant]) REFERENCES [dbo].[user] ([idUser]),
    CONSTRAINT [FK_heritage_user1] FOREIGN KEY ([parent1]) REFERENCES [dbo].[user] ([idUser]),
    CONSTRAINT [FK_heritage_user2] FOREIGN KEY ([parent2]) REFERENCES [dbo].[user] ([idUser]),
    CONSTRAINT [FK_heritage_user3] FOREIGN KEY ([parent1]) REFERENCES [dbo].[user] ([idUser])
);

