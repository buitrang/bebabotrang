﻿CREATE TABLE [dbo].[user] (
    [idUser]   INT          NOT NULL,
    [login]    VARCHAR (50) NOT NULL,
    [password] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_user_1] PRIMARY KEY CLUSTERED ([idUser] ASC)
);

