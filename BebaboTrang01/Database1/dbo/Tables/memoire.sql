﻿CREATE TABLE [dbo].[memoire] (
    [idMemoire]       INT          NOT NULL,
    [nomEvenement]    VARCHAR (50) NULL,
    [noteEvenement]   VARCHAR (50) NULL,
    [fk_idPhotoVideo] INT          NULL,
    CONSTRAINT [PK_memoire] PRIMARY KEY CLUSTERED ([idMemoire] ASC)
);

