﻿CREATE TABLE [dbo].[photoVideo] (
    [idPhotoVideo] INT          NOT NULL,
    [lien]         VARCHAR (50) NULL,
    [legendePhoto] VARCHAR (50) NULL,
    CONSTRAINT [PK_photoVideo] PRIMARY KEY CLUSTERED ([idPhotoVideo] ASC)
);

