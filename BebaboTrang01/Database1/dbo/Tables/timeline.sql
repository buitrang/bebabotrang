﻿CREATE TABLE [dbo].[timeline] (
    [idTimeline]           INT NOT NULL,
    [fk_idCourbe]          INT NULL,
    [fk_idPiqueVaccin]     INT NULL,
    [fk_idMemoire]         INT NULL,
    [fk_idAllergieMaladie] INT NULL,
    [fk_idNaissance]       INT NULL,
    CONSTRAINT [PK_timeline] PRIMARY KEY CLUSTERED ([idTimeline] ASC),
    CONSTRAINT [FK_timeline_allergieMaladie] FOREIGN KEY ([fk_idAllergieMaladie]) REFERENCES [dbo].[allergieMaladie] ([idAllergieMaladie]),
    CONSTRAINT [FK_timeline_courbe] FOREIGN KEY ([fk_idCourbe]) REFERENCES [dbo].[courbe] ([idCourbe]),
    CONSTRAINT [FK_timeline_memoire] FOREIGN KEY ([fk_idMemoire]) REFERENCES [dbo].[memoire] ([idMemoire])
);

