﻿CREATE TABLE [dbo].[chroniquePonctuelleAllergie] (
    [idChroniquePonctuelleAllergie] INT          NOT NULL,
    [nom]                           VARCHAR (50) NULL,
    CONSTRAINT [PK_chroniquePonctuelleAllergie] PRIMARY KEY CLUSTERED ([idChroniquePonctuelleAllergie] ASC)
);

