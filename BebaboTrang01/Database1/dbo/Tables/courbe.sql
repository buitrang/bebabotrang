﻿CREATE TABLE [dbo].[courbe] (
    [poids]            TINYINT NULL,
    [idCourbe]         INT     NOT NULL,
    [taille]           INT     NULL,
    [dateMesure]       DATE    NULL,
    [perimetreCranien] INT     NULL,
    CONSTRAINT [PK_courbe] PRIMARY KEY CLUSTERED ([idCourbe] ASC)
);

