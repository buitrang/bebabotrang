USE [master]
GO
/****** Object:  Database [bebabo]    Script Date: 26-01-18 17:22:26 ******/
CREATE DATABASE [bebabo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'bebabo', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL11.MSSQLSERVER2\MSSQL\DATA\bebabo.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'bebabo_log', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL11.MSSQLSERVER2\MSSQL\DATA\bebabo_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [bebabo] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bebabo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bebabo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bebabo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bebabo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bebabo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bebabo] SET ARITHABORT OFF 
GO
ALTER DATABASE [bebabo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [bebabo] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [bebabo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bebabo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bebabo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bebabo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bebabo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bebabo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bebabo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bebabo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bebabo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [bebabo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bebabo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bebabo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bebabo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bebabo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bebabo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bebabo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bebabo] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [bebabo] SET  MULTI_USER 
GO
ALTER DATABASE [bebabo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bebabo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bebabo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bebabo] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [bebabo]
GO
/****** Object:  Table [dbo].[courbe]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[courbe](
	[poids] [int] IDENTITY(1,1) NOT NULL,
	[idCourbe] [int] NOT NULL,
	[taille] [int] NULL,
	[dateMesure] [datetime] NOT NULL,
	[perimetreCranien] [int] NULL,
	[fk_idProfil] [int] NULL,
 CONSTRAINT [PK_courbe] PRIMARY KEY CLUSTERED 
(
	[idCourbe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[duree]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[duree](
	[idDuree] [int] NOT NULL,
	[duree] [int] NULL,
 CONSTRAINT [PK_duree] PRIMARY KEY CLUSTERED 
(
	[idDuree] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fois]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fois](
	[idFois] [int] NOT NULL,
	[numbreFois] [int] NULL,
 CONSTRAINT [PK_fois] PRIMARY KEY CLUSTERED 
(
	[idFois] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[genre]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[genre](
	[idGenre] [int] NOT NULL,
	[genre] [varchar](50) NULL,
 CONSTRAINT [PK_genre] PRIMARY KEY CLUSTERED 
(
	[idGenre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[maladie]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[maladie](
	[idMaladie] [int] NOT NULL,
	[nomMaladie] [varchar](100) NULL,
 CONSTRAINT [PK_maladie] PRIMARY KEY CLUSTERED 
(
	[idMaladie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MaladieVaccinLot]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaladieVaccinLot](
	[idMaladieVaccinLot] [int] NOT NULL,
	[fk_idMaladie] [int] NULL,
	[fk_idVaccinLot] [int] NULL,
 CONSTRAINT [PK_MaladieVaccinLot] PRIMARY KEY CLUSTERED 
(
	[idMaladieVaccinLot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[medicament]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[medicament](
	[idMedicament] [int] NOT NULL,
	[nomMedicament] [varchar](50) NULL,
 CONSTRAINT [PK_medicament] PRIMARY KEY CLUSTERED 
(
	[idMedicament] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[memoire]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[memoire](
	[idMemoire] [int] NOT NULL,
	[nomEvenement] [varchar](50) NULL,
	[noteEvenement] [varchar](50) NULL,
	[fk_idPhotoVideo] [int] NULL,
	[fk_idProfil] [int] NULL,
 CONSTRAINT [PK_memoire] PRIMARY KEY CLUSTERED 
(
	[idMemoire] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[naissance]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[naissance](
	[idNaissance] [int] NOT NULL,
	[lieuNaissance] [varchar](50) NULL,
	[noteNaissance] [varchar](50) NULL,
	[fk_idTerme] [int] NULL,
	[fk_idTypeAccouchement] [int] NULL,
	[fk_idTest] [int] NULL,
	[fk_idProfil] [int] NULL,
 CONSTRAINT [PK_naissance] PRIMARY KEY CLUSTERED 
(
	[idNaissance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[photoVideo]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[photoVideo](
	[idPhotoVideo] [int] NOT NULL,
	[lien] [varchar](50) NULL,
	[legendePhoto] [varchar](50) NULL,
 CONSTRAINT [PK_photoVideo] PRIMARY KEY CLUSTERED 
(
	[idPhotoVideo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[piqureVaccin]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[piqureVaccin](
	[idPiqureVaccin] [int] NOT NULL,
	[fk_idVaccinLotFoisDuree] [int] NULL,
	[fk_idProfil] [int] NULL,
 CONSTRAINT [PK_piqureVaccin] PRIMARY KEY CLUSTERED 
(
	[idPiqureVaccin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[profil]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[profil](
	[idProfil] [int] IDENTITY(1,1) NOT NULL,
	[nom] [varchar](50) NULL,
	[prenom] [varchar](50) NULL,
	[ddn] [date] NULL,
	[fk_idGenre] [int] NULL,
 CONSTRAINT [PK_timeline] PRIMARY KEY CLUSTERED 
(
	[idProfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[profilUserRelation]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[profilUserRelation](
	[idProfilUserRelation] [int] NOT NULL,
	[fk_idProfil] [int] NULL,
	[fk_idUser] [int] NULL,
	[fk_idRelation] [int] NULL,
 CONSTRAINT [PK_profilUserRelation] PRIMARY KEY CLUSTERED 
(
	[idProfilUserRelation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[relation]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[relation](
	[idRelation] [int] NOT NULL,
	[libelle] [varchar](50) NULL,
 CONSTRAINT [PK_relation] PRIMARY KEY CLUSTERED 
(
	[idRelation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roles]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roles](
	[idRole] [int] NOT NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[idRole] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[terme]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[terme](
	[idTerme] [int] NOT NULL,
	[nombreSemaine] [int] NULL,
	[noteTerme] [varchar](50) NULL,
 CONSTRAINT [PK_terme] PRIMARY KEY CLUSTERED 
(
	[idTerme] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[test]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[test](
	[idTest] [int] NOT NULL,
	[nomTest] [varchar](50) NULL,
	[resultat] [varchar](100) NULL,
 CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED 
(
	[idTest] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[traitement]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[traitement](
	[idTraitement] [int] NOT NULL,
	[noteTraitement] [varchar](100) NULL,
	[dateDebut] [datetime] NULL,
	[dateFin] [datetime] NULL,
	[fk_idMedicament] [int] NULL,
 CONSTRAINT [PK_traitement] PRIMARY KEY CLUSTERED 
(
	[idTraitement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[traitementMaladie]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[traitementMaladie](
	[idTraitementMaladie] [int] NOT NULL,
	[fk_idMaladie] [int] NULL,
	[dateMaladie] [date] NULL,
	[fk_idTraitementMedicament] [int] NULL,
	[type] [varchar](50) NULL,
	[fk_idProfil] [int] NULL,
 CONSTRAINT [PK_traitementMaladie] PRIMARY KEY CLUSTERED 
(
	[idTraitementMaladie] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[traitementMedicament]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[traitementMedicament](
	[idTraitementMedicament] [int] NOT NULL,
	[fk_idTraitement] [int] NULL,
	[fk_idMedicament] [int] NULL,
 CONSTRAINT [PK_traitementMedicament] PRIMARY KEY CLUSTERED 
(
	[idTraitementMedicament] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[typeAccouchement]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[typeAccouchement](
	[idTypeAccouchement] [int] NOT NULL,
	[nomTypeAccouchement] [varchar](50) NULL,
 CONSTRAINT [PK_typeAccouchement] PRIMARY KEY CLUSTERED 
(
	[idTypeAccouchement] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[login] [varchar](50) NULL,
	[password] [varchar](50) NULL,
 CONSTRAINT [PK_user_1] PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userRoleProfil]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userRoleProfil](
	[idUserRoleProfil] [int] IDENTITY(1,1) NOT NULL,
	[fk_idUser] [int] NULL,
	[fk_idRole] [int] NULL,
	[fk_idProfil] [int] NULL,
 CONSTRAINT [PK_userRoleProfil] PRIMARY KEY CLUSTERED 
(
	[idUserRoleProfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VaccinLot]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VaccinLot](
	[idVaccinLot] [int] NOT NULL,
	[nomVaccin] [varchar](50) NULL,
	[refVaccin] [varchar](50) NULL,
 CONSTRAINT [PK_VaccinLot] PRIMARY KEY CLUSTERED 
(
	[idVaccinLot] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vaccinLotFoisDuree]    Script Date: 26-01-18 17:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vaccinLotFoisDuree](
	[idVaccinLotFoisDuree] [int] NOT NULL,
	[fk_idVaccinLot] [int] NULL,
	[fk_idFois] [int] NULL,
	[fk_idDuree] [int] NULL,
 CONSTRAINT [PK_vaccinLotFoisDuree] PRIMARY KEY CLUSTERED 
(
	[idVaccinLotFoisDuree] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[courbe]  WITH CHECK ADD  CONSTRAINT [FK_courbe_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[courbe] CHECK CONSTRAINT [FK_courbe_profil]
GO
ALTER TABLE [dbo].[MaladieVaccinLot]  WITH CHECK ADD  CONSTRAINT [FK_MaladieVaccinLot_maladie] FOREIGN KEY([fk_idMaladie])
REFERENCES [dbo].[maladie] ([idMaladie])
GO
ALTER TABLE [dbo].[MaladieVaccinLot] CHECK CONSTRAINT [FK_MaladieVaccinLot_maladie]
GO
ALTER TABLE [dbo].[MaladieVaccinLot]  WITH CHECK ADD  CONSTRAINT [FK_MaladieVaccinLot_VaccinLot] FOREIGN KEY([fk_idVaccinLot])
REFERENCES [dbo].[VaccinLot] ([idVaccinLot])
GO
ALTER TABLE [dbo].[MaladieVaccinLot] CHECK CONSTRAINT [FK_MaladieVaccinLot_VaccinLot]
GO
ALTER TABLE [dbo].[memoire]  WITH CHECK ADD  CONSTRAINT [FK_memoire_photoVideo] FOREIGN KEY([fk_idPhotoVideo])
REFERENCES [dbo].[photoVideo] ([idPhotoVideo])
GO
ALTER TABLE [dbo].[memoire] CHECK CONSTRAINT [FK_memoire_photoVideo]
GO
ALTER TABLE [dbo].[memoire]  WITH CHECK ADD  CONSTRAINT [FK_memoire_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[memoire] CHECK CONSTRAINT [FK_memoire_profil]
GO
ALTER TABLE [dbo].[naissance]  WITH CHECK ADD  CONSTRAINT [FK_naissance_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[naissance] CHECK CONSTRAINT [FK_naissance_profil]
GO
ALTER TABLE [dbo].[naissance]  WITH CHECK ADD  CONSTRAINT [FK_naissance_terme] FOREIGN KEY([fk_idTerme])
REFERENCES [dbo].[terme] ([idTerme])
GO
ALTER TABLE [dbo].[naissance] CHECK CONSTRAINT [FK_naissance_terme]
GO
ALTER TABLE [dbo].[naissance]  WITH CHECK ADD  CONSTRAINT [FK_naissance_test] FOREIGN KEY([fk_idTest])
REFERENCES [dbo].[test] ([idTest])
GO
ALTER TABLE [dbo].[naissance] CHECK CONSTRAINT [FK_naissance_test]
GO
ALTER TABLE [dbo].[naissance]  WITH CHECK ADD  CONSTRAINT [FK_naissance_typeAccouchement] FOREIGN KEY([fk_idTypeAccouchement])
REFERENCES [dbo].[typeAccouchement] ([idTypeAccouchement])
GO
ALTER TABLE [dbo].[naissance] CHECK CONSTRAINT [FK_naissance_typeAccouchement]
GO
ALTER TABLE [dbo].[piqureVaccin]  WITH CHECK ADD  CONSTRAINT [FK_piqureVaccin_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[piqureVaccin] CHECK CONSTRAINT [FK_piqureVaccin_profil]
GO
ALTER TABLE [dbo].[piqureVaccin]  WITH CHECK ADD  CONSTRAINT [FK_piqureVaccin_vaccinLotFoisDuree] FOREIGN KEY([fk_idVaccinLotFoisDuree])
REFERENCES [dbo].[vaccinLotFoisDuree] ([idVaccinLotFoisDuree])
GO
ALTER TABLE [dbo].[piqureVaccin] CHECK CONSTRAINT [FK_piqureVaccin_vaccinLotFoisDuree]
GO
ALTER TABLE [dbo].[profil]  WITH CHECK ADD  CONSTRAINT [FK_profil_genre] FOREIGN KEY([fk_idGenre])
REFERENCES [dbo].[genre] ([idGenre])
GO
ALTER TABLE [dbo].[profil] CHECK CONSTRAINT [FK_profil_genre]
GO
ALTER TABLE [dbo].[profilUserRelation]  WITH CHECK ADD  CONSTRAINT [FK_profilUserRelation_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[profilUserRelation] CHECK CONSTRAINT [FK_profilUserRelation_profil]
GO
ALTER TABLE [dbo].[profilUserRelation]  WITH CHECK ADD  CONSTRAINT [FK_profilUserRelation_relation] FOREIGN KEY([fk_idRelation])
REFERENCES [dbo].[relation] ([idRelation])
GO
ALTER TABLE [dbo].[profilUserRelation] CHECK CONSTRAINT [FK_profilUserRelation_relation]
GO
ALTER TABLE [dbo].[profilUserRelation]  WITH CHECK ADD  CONSTRAINT [FK_profilUserRelation_user] FOREIGN KEY([fk_idUser])
REFERENCES [dbo].[user] ([idUser])
GO
ALTER TABLE [dbo].[profilUserRelation] CHECK CONSTRAINT [FK_profilUserRelation_user]
GO
ALTER TABLE [dbo].[traitementMaladie]  WITH CHECK ADD  CONSTRAINT [FK_traitementMaladie_maladie] FOREIGN KEY([fk_idMaladie])
REFERENCES [dbo].[maladie] ([idMaladie])
GO
ALTER TABLE [dbo].[traitementMaladie] CHECK CONSTRAINT [FK_traitementMaladie_maladie]
GO
ALTER TABLE [dbo].[traitementMaladie]  WITH CHECK ADD  CONSTRAINT [FK_traitementMaladie_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[traitementMaladie] CHECK CONSTRAINT [FK_traitementMaladie_profil]
GO
ALTER TABLE [dbo].[traitementMaladie]  WITH CHECK ADD  CONSTRAINT [FK_traitementMaladie_traitementMedicament] FOREIGN KEY([fk_idTraitementMedicament])
REFERENCES [dbo].[traitementMedicament] ([idTraitementMedicament])
GO
ALTER TABLE [dbo].[traitementMaladie] CHECK CONSTRAINT [FK_traitementMaladie_traitementMedicament]
GO
ALTER TABLE [dbo].[traitementMedicament]  WITH CHECK ADD  CONSTRAINT [FK_traitementMedicament_medicament] FOREIGN KEY([fk_idMedicament])
REFERENCES [dbo].[medicament] ([idMedicament])
GO
ALTER TABLE [dbo].[traitementMedicament] CHECK CONSTRAINT [FK_traitementMedicament_medicament]
GO
ALTER TABLE [dbo].[traitementMedicament]  WITH CHECK ADD  CONSTRAINT [FK_traitementMedicament_traitement] FOREIGN KEY([fk_idTraitement])
REFERENCES [dbo].[traitement] ([idTraitement])
GO
ALTER TABLE [dbo].[traitementMedicament] CHECK CONSTRAINT [FK_traitementMedicament_traitement]
GO
ALTER TABLE [dbo].[userRoleProfil]  WITH CHECK ADD  CONSTRAINT [FK_userRoleProfil_profil] FOREIGN KEY([fk_idProfil])
REFERENCES [dbo].[profil] ([idProfil])
GO
ALTER TABLE [dbo].[userRoleProfil] CHECK CONSTRAINT [FK_userRoleProfil_profil]
GO
ALTER TABLE [dbo].[userRoleProfil]  WITH CHECK ADD  CONSTRAINT [FK_userRoleProfil_roles] FOREIGN KEY([fk_idRole])
REFERENCES [dbo].[roles] ([idRole])
GO
ALTER TABLE [dbo].[userRoleProfil] CHECK CONSTRAINT [FK_userRoleProfil_roles]
GO
ALTER TABLE [dbo].[userRoleProfil]  WITH CHECK ADD  CONSTRAINT [FK_userRoleProfil_user] FOREIGN KEY([fk_idUser])
REFERENCES [dbo].[user] ([idUser])
GO
ALTER TABLE [dbo].[userRoleProfil] CHECK CONSTRAINT [FK_userRoleProfil_user]
GO
ALTER TABLE [dbo].[vaccinLotFoisDuree]  WITH CHECK ADD  CONSTRAINT [FK_vaccinLotFoisDuree_duree] FOREIGN KEY([fk_idDuree])
REFERENCES [dbo].[duree] ([idDuree])
GO
ALTER TABLE [dbo].[vaccinLotFoisDuree] CHECK CONSTRAINT [FK_vaccinLotFoisDuree_duree]
GO
ALTER TABLE [dbo].[vaccinLotFoisDuree]  WITH CHECK ADD  CONSTRAINT [FK_vaccinLotFoisDuree_fois] FOREIGN KEY([fk_idFois])
REFERENCES [dbo].[fois] ([idFois])
GO
ALTER TABLE [dbo].[vaccinLotFoisDuree] CHECK CONSTRAINT [FK_vaccinLotFoisDuree_fois]
GO
ALTER TABLE [dbo].[vaccinLotFoisDuree]  WITH CHECK ADD  CONSTRAINT [FK_vaccinLotFoisDuree_VaccinLot] FOREIGN KEY([fk_idVaccinLot])
REFERENCES [dbo].[VaccinLot] ([idVaccinLot])
GO
ALTER TABLE [dbo].[vaccinLotFoisDuree] CHECK CONSTRAINT [FK_vaccinLotFoisDuree_VaccinLot]
GO
USE [master]
GO
ALTER DATABASE [bebabo] SET  READ_WRITE 
GO
